/*
	This function orders a number to change it in descendent order.

*/

export function kata1 (number) {
	
	// If number is not a number, return false.

	if(!parseInt(number)){
		return false;
	}

	// Now we are sure that our number is really a number, we're gonna create an array of strings with it. Sort it, and parse it. 

	let arrayOfNumbers = number.toString().split("");
	arrayOfNumbers.sort((a, b)=> b-a)

	return parseInt(arrayOfNumbers.join(""));
	
	// We're done !
}
