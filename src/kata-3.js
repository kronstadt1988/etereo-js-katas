/*
 This function return the numbers where the sum of the quadratic divisors of a number, is also a quadratic.

*/

export function kata3 (n, m) {
	let solution = [];

	// A function that given a number, returns an array of its divisors.

	function findDivisorsOfNumber (number){
		let arrayOfDivisors = [];
		for(let i = 1; i <= number; i ++){
			if(number % i === 0){
				arrayOfDivisors.push(i);
			}
			continue;
		}
		return arrayOfDivisors;
	}

	// Given an array of numbers, it returns the sum of all their squares.

	function performQuadraticSum(array){
		return array.reduce((sum, value)=>{
			return sum + value*value;
		}, 0);
	}

	// Given a number, returns the square root or false if it's not an integer.

	function numberIsAPower(number){
		if(n < 1 ){
			return false;
		}

		if(Math.sqrt(number) % 1 === 0){
			return Math.sqrt(number);
		}

		return false;
	}

	/*

		For each number in the given range:
			1- Find all divisors.
			2- Sum all their squares.
			3- If the sum of their squares is also a square, push it to the solution array.
			4- Return the solution array.
	*/

	for(let i = n; i<=m; i++){

		let divisors = findDivisorsOfNumber(i);
		let quadraticSumOfDivisors = performQuadraticSum(divisors);

		if(numberIsAPower(quadraticSumOfDivisors)){
			solution.push([i, quadraticSumOfDivisors])
		}
	}
	return solution

}
