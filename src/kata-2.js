/*
	This function gets access to an object
*/

export function kata2 (obj, def, path) {

	// We create the function we want to return in case we don't pass the third argument.

	function getPathToProperty(path){
		const pathToNavigate = path.split("."); 
		let currentNode = obj;

		for(let i = 0; i < pathToNavigate.length; i++){
			/*
				Navigate to a deeper level while we still have a property to navigate from pathToNavigate.
			*/
			if(currentNode[pathToNavigate[i]]){
				currentNode = currentNode[pathToNavigate[i]];	
			}else{
				// Property doesn't exist
				currentNode = null;
				break;
			}
			
		}

		if(currentNode){
			return currentNode;
		}
		return def;
	}

	// If path is passed, return its value.

	if(path){
		return getPathToProperty(path)
	}
	// If not, return the accessor value, so the user can use it.
	return getPathToProperty;
}